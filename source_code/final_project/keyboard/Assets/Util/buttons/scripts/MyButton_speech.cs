﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MyButton_speech:MyButton
{
    keyboard_manager_speech keyboard;

    public void Awake()
    {
        base.Awake();
        keyboard= GameObject.Find("keyboard").GetComponent<keyboard_manager_speech>();
        onClick.AddListener(delegate() { writeKey(); });

    }
    public void writeKey()
    {

              keyboard.WriteKey(GetComponentInChildren<Text>());
    }
}

