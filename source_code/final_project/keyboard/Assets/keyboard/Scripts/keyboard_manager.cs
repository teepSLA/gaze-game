﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class keyboard_manager : MonoBehaviour
{

	public GameObject[] panels;
	public autoCompletion ac;

    [HideInInspector]
	public int position;
	

////??? 
    public bool isActive = false;
    public bool isActivated;


    void Start()
    {
        Time.timeScale = 1; 
        position=0;
        updateAC();
        init();
    }
    public virtual void init(){}
    private void Update()
    {

        transform.localScale = new Vector3(0.0007f * Screen.width - 0.0302f, 0.0012f * Screen.height , 1.0f);
        setup();
    }
    public virtual void setup(){}
    public void SetKeyboardType(int n)
    {
    	switch (n)
    	{
    		case 0:
    		panels[0].SetActive(true);
    		panels[1].SetActive(false);
    		panels[2].SetActive(false);
    		break;
    		case 1:
    		panels[0].SetActive(false);
    		panels[1].SetActive(true);
    		panels[2].SetActive(false);
    		break;
    		case 2:
    		panels[0].SetActive(false);
    		panels[1].SetActive(false);
    		panels[2].SetActive(true);
    		break;
    	}
    updateAC();
    }

   public virtual void updateAC(){}
   public virtual void WriteKey(Text t){}
   public virtual void writeWord(string text){}
   public virtual void send(){}
   public virtual void deleteWord(){}
   public virtual void deleteLetter(){}
   public virtual void clear(){}
   public virtual void arrows(string direction){}


}
