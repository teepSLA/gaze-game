﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using UnityEngine.SceneManagement;
using System.Drawing;
using howto_bounding_circle;
using System.Linq;

public class move : MonoBehaviour
{

    public List < PointF > lookingPoints;
    public List < GameObject > debugVisualise;

    public List < Vector2 > offsetsTmp;

    string name = "";
    // Use this for initialization
    float z=0;
    int index=0;
    bool hasStopped= false;
      UnityEngine.Color colorIni = UnityEngine.Color.white;
      UnityEngine.Color colorFin = UnityEngine.Color.green;
      float duration = 3f;
     UnityEngine.Color lerpedColor = UnityEngine.Color.white;
  //   public static Vector2[]  positionsBall = new Vector2[5] {new Vector2(0,0),new Vector2(-120,0.0133f*Screen.height+45.0f),new Vector2(120,0.0133f*Screen.height+45.0f), new Vector2(-120,-(0.0133f*Screen.height+45.0f)),new Vector2(120,-(0.0133f*Screen.height+45.0f))};
     public static Vector2[]  positionsBall = new Vector2[5] {new Vector2(0,0),new Vector2(-120,0.0133f*Screen.height+45.0f),new Vector2(120,0.0133f*Screen.height+45.0f), new Vector2(-120,-(0.0133f*Screen.height+45.0f)),new Vector2(120,-(0.0133f*Screen.height+45.0f))};

      float t=0;
      bool flag; 
    public void Start()
    {
        positionsBall = new Vector2[5] {new Vector2(0,0),new Vector2(10,Screen.height-10),new Vector2(Screen.width -10 ,Screen.height-10),new Vector2(10,10),new Vector2(Screen.width-10,10)};

        reset();
        offsetsTmp = new List < Vector2 >();
        transform.localPosition= new Vector2(0,0);//positionsBall[0];            

    }
    public void Update()
    {
         lerpedColor = UnityEngine.Color.Lerp(colorIni, colorFin,  t);
         this.GetComponent<Renderer>().material.color = lerpedColor;
 
         if (flag == true) {
             t -= Time.deltaTime/duration;
             if (t < 0.01f)
                 flag = false;
         } else {
             t += Time.deltaTime/duration;
             if (t > 0.99f)
                 flag = true;
         }

        if(GameObject.Find("gazeSphere"))
        {
       //     GameObject.Find("gazeSphere").SetActive(false);
        }
    }
    // Update is called once per frame
    public void turnAndAdd( Vector3 gaze)
    {
                   /*        RaycastHit hitEye,hitBall;

                        if (Physics.Raycast(Camera.main.ScreenPointToRay(gaze), out hitEye, float.PositiveInfinity, LayerMask.GetMask("Background")))
                    {
                            print(hitEye.point);
                    }*/
                lookingPoints.Add(new PointF(gaze.x,gaze.y));
                z=gaze.z;
                        // center, top left, top right, bottom left, bottom right

         //positionsBall = new Vector2[5] {new Vector2(0,0),new Vector2(-100,45), new Vector2(100,45), new Vector2(-100,-45),new Vector2(100,-45)};

    }


    public void execute()
    {

                          Ray camRay;
                    RaycastHit hit;
                 /*   // VISUALISTAION looking points
                  for(int i=0;i< debugVisualise.Count;i++)
                    {
                        Destroy(debugVisualise[i]);
                    }
                    debugVisualise= new List < GameObject >();

  
                    for (int i = 0; i<lookingPoints.Count; i++)
                    {
                        camRay  = Camera.main.ScreenPointToRay(new Vector3(lookingPoints[i].X,lookingPoints[i].Y,0));                 

                        if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                        {
                               GameObject s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            s.transform.position = hit.point;
                            debugVisualise.Add(s);
                        }
                    }
//*/ 


                    PointF center;
                    float radius;

                   // this.GetComponent<Renderer>().enabled=false;
                   List<PointF> l =  Util.copyList(lookingPoints);
                   l= Util.removeOutsiders( l,lookingPoints.Count);

                    Geometry.FindMinimalBoundingCircle(l, out  center, out  radius);
                 //   Vector3 centeredPos = new Vector3(center.X- GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[0][0], center.Y-GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[0][1]);

                    Vector2 v  = GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[index];
                    PointF centerFixed ;
                        centerFixed = new PointF(center.X-v[0],center.Y- v[1]);


/*    // VISUALISATION filtered points
                    for (int i = 0; i<l.Count; i++)
                    {
                        camRay  = Camera.main.ScreenPointToRay(new Vector3(l[i].X,l[i].Y,0));                 

                        if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                        {
                               GameObject s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            s.transform.position = hit.point;
                            s.GetComponent<Renderer>().material.color = UnityEngine.Color.red;
                             s.layer = LayerMask.NameToLayer("gaze");
                            debugVisualise.Add(s);

                        }
                    }
//*/

                    /*
                    //////////////////////////////// 
                    /////       display 1 sphere at the barycenter, and the other sphere at the point the more far away. can be useful to visualise radius
                    ///////////
                    if(!hasStopped)
                    {
                        hasStopped= true;
                        float d =-1;

                        int index=-1;
                        for(int i=0;i< lookingPoints.Count; i++ )
                        {
                            print(Util.GetDistance(center,lookingPoints[i]));
                            if(Util.GetDistance(center,lookingPoints[i])> d )
                            {
                                d= Util.GetDistance(center,lookingPoints[i]);
                                index = i;
                            }
                        }
                        Ray camRay;
                        RaycastHit hit;
                       camRay  = Camera.main.ScreenPointToRay(new Vector3(center.X,center.Y,0));                 

                    if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                    {
                         GameObject.Find("Sphere1").transform.position = hit.point;//Smoothify3(pointFixed,0.99f); 
                    }

                       camRay  = Camera.main.ScreenPointToRay(new Vector3(lookingPoints[index].X,lookingPoints[index].Y,0));                 

                    if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                    {
                         GameObject.Find("Sphere2").transform.position = hit.point;//Smoothify3(pointFixed,0.99f); 
                    }
                      

                        //GameObject.Find("Sphere1").transform.position = new Vector3(center.Y,center.Y,z);
                        //GameObject.Find("Sphere2").transform.position = new Vector3(fp.X,fp.Y,z);
                        print("radius= "+ radius);
                        print("index= "+ index);
                        print("d= "+ d);


                    }
*/

 
                    /*////////////
                    ///  
                    //// display the circle at the barycenter
                    ///
                    //////////////////
                                
                       camRay  = Camera.main.ScreenPointToRay(new Vector3(centerFixed.X,centerFixed.Y,0));                 

                    if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                    {
                        print("find");
                         GameObject.Find("gaze").transform.position = hit.point;//Smoothify3(pointFixed,0.99f); 
                    }
            //*/
               //     GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().transform.position = new Vector3(center.X,center.Y,z);//Smoothify3(pointFixed,0.99f); 

                    RaycastHit hitEye,hitBall;
                    Vector3 ballPos = Camera.main.WorldToScreenPoint(this.transform.position);
                    lookingPoints=new List<PointF>();

                            print("distance:"+Util.GetDistance( new PointF(ballPos.x,ballPos.y),centerFixed));
                            // if the position is good enough
                            if(Util.GetDistance( new PointF(ballPos.x, ballPos.y),centerFixed)< 7)
                            {
                                index++;
                                //if(index <9)
                                if(index<5) // once we have calibrated all the corners 
                                {

                                    offsetsTmp= new List<Vector2>();

                                    camRay  = Camera.main.ScreenPointToRay(positionsBall[index]);                 

                                        if (Physics.Raycast(camRay, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
                                        {
                  
                                     transform.position= hit.point;       

                                         }
                                }
                                else //end of calibration go back to menu
                                {

                                    savoffsets();

                                    //Application.LoadLevel("menu");
                                            SceneManager.LoadScene("menu");

                                    //Application.LoadLevel("test");
                                }
                                print("found");
                            }
                            else
                            {
                                    GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[index]= new Vector2(center.X - ballPos.x,   center.Y  - ballPos.y );
    
                                GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().newOffset = true;
                            }
                    /*// use the mean of offsets and reset every 2 secs 
                    RaycastHit hitEye,hitBall;
                    Vector3 centerPosition = new Vector3(center.X, center.Y, z);
                    Vector3 ballPos = Camera.main.WorldToScreenPoint(this.transform.position);
                    lookingPoints=new List<PointF>();

                            print("distance:"+Util.GetDistance( new PointF(ballPos.x,ballPos.y),centerFixed));
                            // if the position is good enough
                            if(Util.GetDistance( new PointF(ballPos.x, ballPos.y),centerFixed)< 7)
                            {
                                index++;
                                //if(index <9)
                                if(index<5)
                                {
                                    offsetsTmp= new List<Vector2>();
                                     transform.localPosition= positionsBall[index];            
                                }
                                else //end of calibration go back to menu
                                {
                                    GameObject.Find("settings").GetComponent<settings>().sp.offsets = GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets;
                                    Application.LoadLevel("menu");
                                }
                            }
                            else
                            {
                                offsetsTmp.Add(new Vector2(center.X - ballPos.x,center.Y - ballPos.y ));
                                GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[index]= getMeanVector(offsetsTmp);
                                GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().newOffset = true;
                            }
                      //*/

                            // keep record of all of the points and recalculate the new barycenter each time 
                            /*
                            RaycastHit hitEye,hitBall;
                            Vector3 centerPosition = new Vector3(center.X, center.Y, z);
                            Vector3 ballPos = Camera.main.WorldToScreenPoint(this.transform.position);

                            print("distance:"+Util.GetDistance( new PointF(ballPos.x,ballPos.y),centerFixed));
                            // if the position is good enough
                            if(Util.GetDistance( new PointF(ballPos.x, ballPos.y),centerFixed)< 7)
                            {
                                index++;
                                if(index <9)
                                {
                                   // offsetsTmp= new List<Vector2>();
                                     transform.localPosition= positionsBall[index];            
                                }
                                else //end of calibration go back to menu
                                {
                                    GameObject.Find("settings").GetComponent<settings>().sp.offsets = GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets;
                                    Application.LoadLevel("menu");
                                }
                            }
                            else
                            {
                                GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[index]= new Vector2(center.X - ballPos.x,center.Y - ballPos.y );
                                GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().newOffset = true;
                            }
                        */
                            
    }
    public void reset()
    {
        lookingPoints=new List<PointF>();
    }

    Vector2 getMeanVector(List<Vector2> offsetsTmp)
    {
        Vector2 mean = new Vector2(0,0);
        for(int i=0;i<offsetsTmp.Count;i++)
        {
            mean[0]=mean[0]+offsetsTmp[i][0];
            mean[1]=mean[1]+offsetsTmp[i][1];
        }
        mean[0]= mean[0]/offsetsTmp.Count;
        mean[1]= mean[1]/offsetsTmp.Count;
        return mean;
    }
    public void savoffsets()
    {

        float meanx =0;
        float meany = 0;

        for(int i=0;i<5;i++)
        {
                    print("avant"+GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][1]);

         meanx = meanx + GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][0];
         meany = meany + GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][1];
        }
        Vector2 means = new Vector2(meanx/5.0f,meany/5.0f);


        for(int i=0;i<5;i++)
        {
          GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][0]= GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][0] - means[0];
          GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][1]= GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][1] - means[1];
                    print("apres"+GameObject.Find("eyeTracker").GetComponent<eye_tracker_menu>().offsets[i][1]);
        }

        print("means"+means[1]);
     

    }
}