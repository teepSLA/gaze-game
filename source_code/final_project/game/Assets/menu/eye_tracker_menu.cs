﻿


using System.Collections;
using System.Collections.Generic;

using System.Collections;
using System.Diagnostics;
using System;

using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;
using UnityEngine.SceneManagement;
using System;

public class eye_tracker_menu : Eye_tracker_interface
{
    float camRayLength = float.PositiveInfinity;
    public float time_to_wait_calibration { get; set; }


    void Start()
    {

        setup();
    
            gazePlot.SetActive(displayBall);

    }

    // Update is called once per frame
    void Update()
    {
        resizeWindow();


        gazeAction();

    }

    public Vector3 Smoothify3(Vector3 point, float FilterSmoothingFactor)
    {
        if (!_hasHistoricPoint)
        {
            _hasHistoricPoint = true;
            _historicPoint = point;
            return point;
        }

        var smoothedPoint = new Vector3(
          point.x * (1.0f - FilterSmoothingFactor) + _historicPoint.x * FilterSmoothingFactor,
          point.y * (1.0f - FilterSmoothingFactor) + _historicPoint.y * FilterSmoothingFactor,
          point.z * (1.0f - FilterSmoothingFactor) + _historicPoint.z * FilterSmoothingFactor);

        _historicPoint = smoothedPoint;
        return smoothedPoint;
    }
    
    public override void getFinalPoint()
    {
        float h = Screen.height;
        float w = Screen.width;
        gazePosition = new Vector3(gazePosition.x - meanOffsets[0], gazePosition.y - meanOffsets[1], 0.0f);

        if (gazePosition.x < w / 2 && gazePosition.y > h / 2)
        {
            index = 1;
        }
        else if ((gazePosition.x > w / 2) && gazePosition.y > h / 2)
        {
            index = 2;
        }
        else if (gazePosition.x < w / 2 && (gazePosition.y < h / 2))
        {
            index = 3;
        }
        else
        {
            index = 4;
        }


        pointFixed = new Vector3(gazePosition.x , gazePosition.y , gazePosition.z);

        if (SceneManager.GetActiveScene().name != "calibration")
        { //this is the menu
            smoothedPoint = Util.Smoothify3(pointFixed, 0.97f)[1];
            toRay = smoothedPoint;
        }
        else // if  if we are in calibration mode dont smoothed 
        {
            smoothedPoint = Util.Smoothify3(gazePosition, 0.97f)[1];
            smoothedFixedPoint = Smoothify3(pointFixed, 0.97f);
            toRay = smoothedFixedPoint;
        }

        if (newOffset)
        {
            camRay = cameraGUI.ScreenPointToRay(pointFixed);
            newOffset = false;
        }
        else
            camRay = cameraGUI.ScreenPointToRay(toRay);
    }

    public override void doActionButton()
    {
                    base.doActionButton(); 


        if (hit.collider.gameObject.name == "browser")
        {
            // Application.LoadLevel("menu_browser"); 
            //SceneManager.LoadScene("menu_browser");
            string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
            PlayerPrefs.SetString("version", lines[10].Split('=')[1]);
            SceneManager.LoadScene("favorite");

        }
        else if (hit.collider.gameObject.name == "speak")
        {
           SceneManager.LoadScene("keyboard");
        }
        else if (hit.collider.gameObject.name == "game")
        {
            SceneManager.LoadScene("play");

           // SceneManager.LoadScene("_Complete-Game");

        }
                else if (hit.collider.gameObject.name == "random")
        {
            PlayerPrefs.SetString("random","true");
           SceneManager.LoadScene("_Complete-Game");
           // SceneManager.LoadScene("_Complete-Game");

        }        else if (hit.collider.gameObject.name == "space")
        {
             PlayerPrefs.SetString("random","false");
            PlayerPrefs.SetString("LoadScene", "_Complete-Game");
           // SceneManager.LoadScene("relax");

            SceneManager.LoadScene("_Complete-Game");

        }
       
        else if (hit.collider.gameObject.name == "Back")
        {
            // Application.LoadLevel("menu"); 
            SceneManager.LoadScene("menu");

        }
                else if (hit.collider.gameObject.name == "zen")
        {
            PlayerPrefs.SetString("LoadScene", "circle");
            SceneManager.LoadScene("relax");

        }
        else if (hit.collider.gameObject.name == "close")
        {
            while (ShowCursor(true) < 0) { }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
      
        else if (hit.collider.gameObject.name.Substring(0, 1) == "v")
        {
            PlayerPrefs.SetString("version", hit.collider.gameObject.name);
            // Application.LoadLevel("favorite");
            SceneManager.LoadScene("favorite");


        }
        else if (hit.collider.gameObject.name == "robot")
        {
            // Application.OpenURL(@"C:\Program Files\Mozilla Firefox\firefox.exe");
            Application.OpenURL("https://teep-sla.eu/");
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, @"C:\Program Files (x86)\OptiKey\OptiKey.exe");
            System.Diagnostics.Process.Start(filePath);
        }
    }

    public override void startConsideringKey()
    {
        if (hit.collider.gameObject.name != "Sphere1")
            timer.enabled = true;

        timer.transform.position = new Vector3(hit.collider.gameObject.transform.position.x, hit.collider.gameObject.transform.position.y, hit.collider.gameObject.transform.position.z);
    }

    public override void ActionsLookNothing()
    {
     
    }
    public override void ActionWhenLookAtSomething()
    {

    }

  public void resizeWindow()
    {
        if(GameObject.Find("wholeWindow"))
        {
            float size = Math.Min(0.00085f * Screen.width, 0.002f * Screen.height);
            GameObject.Find("wholeWindow").transform.localScale = new Vector3(size, size, 1.0f);
        }
    }
}
