﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections; 
using UnityEngine.SceneManagement;

namespace CompleteProject
{
    public class GameOverManager : MonoBehaviour
    {
        public PlayerHealthScript playerHealth;       // Reference to the player's health.
		public GameObject replay;

        Animator anim;                          // Reference to the animator component.
		float timer;


        void Awake ()
        {
		//	replay.SetActive(false);
			timer = 0f;	
        
            // Set up the reference.
            anim = GetComponent <Animator> ();

			
        }


        void Update ()
        {
            /*
			GameObject timeManager  = GameObject.Find("timeManager");
			time_manager tm = timeManager.GetComponent<time_manager>();
            
			//quit application
			if (tm.duration != -1 && tm.timer > tm.duration) {
				 string webplayerQuitURL = "http://google.com";
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
				#elif UNITY_WEBPLAYER
				Application.OpenURL(webplayerQuitURL);
				#else
				Application.Quit();
				#endif

			} else {
				tm.timer += Time.deltaTime; 
				//print ("timer=" + tm.timer);
			}
			// stop the game

    */
            // If the player has run out of health...
            if(playerHealth.currentHealth <= 0)
            {
				timer += Time.deltaTime; 

                // ... tell the animator the game is over.
                anim.SetTrigger ("GameOver");
				if (timer > 1) {

					/*if (PlayerPrefs.GetString ("time")!= "free") {
						//SceneManager.LoadScene ("play");
						SceneManager.LoadScene ("_Complete-Game");

					} else {
						SceneManager.LoadScene ("play");
					}*/

					SceneManager.LoadScene ("play");
				}
            }
        }
    }
}