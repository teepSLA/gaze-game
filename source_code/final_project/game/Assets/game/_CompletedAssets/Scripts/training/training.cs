﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine.UI;

using System;
using System.Collections.Generic;
using UnitySampleAssets.CrossPlatformInput;
using System.IO;

public class training : MonoBehaviour {

	public Text activityLabel;
	float timer;
	Socket sock;
	Socket sock_GSR;
	bool passed = false;
	Socket_manager persistentScript;
	// Use this for initialization
	void Start () {

		  // muse 
		GameObject socket  = GameObject.Find("socket");

		 persistentScript = socket.GetComponent<Socket_manager>();

		//sock = persistentScript.sock;
		 sock_GSR = persistentScript.sock_GSR;

		timer = 0f;	
		//sendSocket (sock,"start",5);
		persistentScript.sendSocket (sock_GSR,"start",5);

	}
	
	// Update is called once per frame
	void Update () {
		/*
		timer += Time.deltaTime; 
		if (timer < 10) {
			activityLabel.text = ((int)(10 - timer)).ToString (); 
		} else if (timer < 20 || (timer > 30 && timer < 40) || (timer > 50 && timer < 60)) {

			activityLabel.text = "CALM";
		} else if (timer < 70) {
			activityLabel.text = "FOCUS";
		} else {
			SceneManager.LoadScene ("_Complete-Game");
		}*/

		timer += Time.deltaTime; 
		if (timer < 30) {
			activityLabel.text = ((int)(30-timer)).ToString (); 
		} else if(!passed){
			passed = true;
			getGSR ();		}

		/*

		if (isCalm ()) {
			activityLabel.text = "CALM";
		} else {
			activityLabel.text = "FOCUS";
		}*/
	}

	bool isCalm()
	{
		byte[] buffer = new byte[10];

		try {

			Stream s = new NetworkStream (sock); 
			StreamReader sr = new StreamReader (s);

			string msg = "";


			for (int j = 0; j < 10; j++) {
				msg += (char)sr.Read ();
			}

			//sendSocket ("ok",2);

			if (msg== "Train/ACTI") {
				return false;
			} else if (msg== "Train/CALM") {
				return true;
			}
			else if(!passed){
				getGSR();
				return true;
				}
			else{
				return true;
			}

		} catch (Exception e) {
			print ("error");
			#if LOG
			Console.WriteLine(e.Message);
			#endif
			return false;
		}	
	}



	void getGSR()
	{
		persistentScript.sendSocket (sock_GSR,"stop",4);
		Stream s_GSR = new NetworkStream (sock_GSR); 
		StreamReader sr_GSR = new StreamReader (s_GSR);

		string msg_GSR = "";

		for (int j = 0; j < 4; j++) {
			msg_GSR += (char)sr_GSR.Read ();
		}
		print (msg_GSR);
		float	gsrMean = float.Parse(msg_GSR);
		msg_GSR = "";

		for (int j = 0; j < 4; j++) {
			msg_GSR += (char)sr_GSR.Read ();
		}
		float	gsrDiff = float.Parse(msg_GSR);
		print (msg_GSR);



		passed = true;
		PlayerPrefs.SetFloat("gsrMean",gsrMean);
		PlayerPrefs.SetFloat("gsrDiff",gsrDiff);


		print("gsrmean = "+gsrMean);
		SceneManager.LoadScene ("play");

	}
		
}
