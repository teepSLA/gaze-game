﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine.UI;

using System;
using System.Collections.Generic;
using System.Linq;

    public class PlayerShoot : MonoBehaviour
    {
        public int damagePerShot = 20;                  // The damage inflicted by each bullet.
        public float timeBetweenBullets = 0.15f;        // The time between each shot.
        public float range = 100f;                      // The distance the gun can fire.

        float timer;                                    // A timer to determine when to fire.
                                                        // Ray shootRay;                                   // A ray from the gun end forwards.
        RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
        int shootableMask;                              // A layer mask so the raycast only hits things on the shootable layer.
        ParticleSystem gunParticles;                    // Reference to the particle system.
        LineRenderer gunLine;                           // Reference to the line renderer.
        AudioSource gunAudio;                           // Reference to the audio source.
        Light gunLight;                                 // Reference to the light component.
        public Light faceLight;								// Duh
        float effectsDisplayTime = 0.2f;                // The proportion of the timeBetweenBullets that the effects will display for.

		public GameObject powerShootUncharged;
		shootingBar shootingBarScript  ;
		void Awake()
        {
            // Create a layer mask for the Shootable layer.
            shootableMask = LayerMask.GetMask("Shootable");

            timer = 0f;
			shootingBarScript =	powerShootUncharged.GetComponent<shootingBar>();
        }


        void Update()
        {
		


            
        }


		void OnTriggerEnter(Collider other)
		{
			if (other.tag == "enemy")
			{
				//print("touched enemy!");

				// Try and find an EnemyHealth script on the gameobject hit.
				 CompleteProject.EnemyHealth enemyHealth = other.GetComponent<CompleteProject.EnemyHealth>() ;
				// If the EnemyHealth component exist...
				  if (enemyHealth != null)
                   {
                       // ... the enemy should take damage.
                       enemyHealth.TakeDamage(damagePerShot, other.transform.position);

                   }
			}
		}

 }



