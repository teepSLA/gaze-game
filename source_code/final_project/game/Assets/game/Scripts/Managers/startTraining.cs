﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnitySampleAssets.CrossPlatformInput;
using System.IO;

public class startTraining : MonoBehaviour {

	public Text buttonText;
	Socket sock;
	public GameObject buttonRelax;
	public GameObject buttonTime;
	public GameObject buttonTraining;
	public GameObject timeManager;
	time_manager tm;
	void Start () {
		// muse 
		/*GameObject socket  = GameObject.Find("socket");

		Socket_manager persistentScript = socket.GetComponent<Socket_manager>();
		sock = persistentScript.sock;
		wait ();*/
		//buttonText.text = " Start training";
		buttonTraining.SetActive(false); 
		tm = timeManager.GetComponent<time_manager>();
		PlayerPrefs.SetString("time","restricted");

	//	string filePath = @"C:\Users\flarradet\Documents\phd\embc2017\experiments\ResExperiment.txt";
		// Example #2: Write one string to a text file.
	//	string text = "";
		// WriteAllText creates a file, writes the specified string to the file,
		// and then closes the file.    You do NOT need to call Flush() or Close().
	//	System.IO.File.WriteAllText(filePath, text);  


	}

	public void trainWithTime()
	{
		tm.duration = 120;
		playWithTime ();
	}

	public void trainWithRelaxation()
	{
		tm.duration = 120;
		playWithRelaxation ();
	}

	public void FreeWithTime()
	{
		PlayerPrefs.SetString("time","free");

		tm.duration = -1;
		playWithTime ();
	}

	public void FreeWithRelax()
	{
		PlayerPrefs.SetString("time","free");
		tm.duration = -1;
		playWithRelaxation ();
	}

	// Update is called once per frame
	public void playWithRelaxation() {

		GameObject socket  = GameObject.Find("socket");

		Socket_manager persistentScript = socket.GetComponent<Socket_manager>();
		persistentScript.connectSocket ();

		PlayerPrefs.SetString("type","relax");

		buttonRelax.SetActive(false);
		buttonTime.SetActive(false);
		buttonTraining.SetActive(true); 

		//SceneManager.LoadScene ("_Complete-Game");

	}
	public void playWithTime() {
		/*
		GameObject socket  = GameObject.Find("socket");

		Socket_manager persistentScript = socket.GetComponent<Socket_manager>();
		persistentScript.connectSocket ();
		//SceneManager.LoadScene ("training");
	//	SceneManager.LoadScene ("_Complete-Game");
		SceneManager.LoadScene ("play");

		PlayerPrefs.SetString("type","time");*/

		GameObject socket  = GameObject.Find("socket");

		Socket_manager persistentScript = socket.GetComponent<Socket_manager>();
		//persistentScript.connectSocket ();

		PlayerPrefs.SetString("type","time");

		buttonRelax.SetActive(false);
		buttonTime.SetActive(false);
		buttonTraining.SetActive(true); 


	}

	public void goTraining()
	{
        //	SceneManager.LoadScene ("training");
        SceneManager.LoadScene ("_Complete-Game");

    }
    bool  wait()
	{
		Stream s = new NetworkStream (sock); 
		StreamReader sr = new StreamReader (s);

		string msg = "";


		for (int j = 0; j < 10; j++) {
			msg += (char)sr.Read ();
		}
		if (msg == "ready2send") {
			return true;
		} else {
			return false;
		}
	}
}
